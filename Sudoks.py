from tkinter import *
from tkinter import font
from random import*
import copy

def joonista_ruut(x1, y1, x2, y2,tahvel):
    return tahvel.create_rectangle(x1,y1, x2, y2, fill="white")

def vasak(event):
    #Kirjutamine
    global loendur
    global järjend_püsiv

    def sulgemine():
        global loendur
        loendur = 0
        tahvel.delete(rida_1)
        tahvel.delete(rida_2)
        tahvel.delete(rida_3)
        tahvel.delete(rida_4)
        aken.destroy()

    def prindi_number():
        global loendur

        silt = vajutatud_ruut + 85
        kirjutatud_nr = teksti_kast.get()

        if kirjutatud_nr not in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
            viga = Label(aken, text = "Kontrolli sisestatud numbrit!", font="calibri 10 bold")
            viga.grid(column=0, row=2, columnspan=2, padx=5, pady=5)
        else:
            aken.destroy()
            tahvel.itemconfigure(silt, text=kirjutatud_nr, font=numbrite_font, fill="blue")
            järjend[rida][element] = kirjutatud_nr

            tahvel.delete(rida_1)
            tahvel.delete(rida_2)
            tahvel.delete(rida_3)
            tahvel.delete(rida_4)
            loendur = 0

    vajutatud_ruut = tahvel.find_withtag(CURRENT)[0]

    if vajutatud_ruut % 9 == 0:
        rida = vajutatud_ruut // 9 - 1
        element = 8
    else:
        rida = vajutatud_ruut // 9
        element = vajutatud_ruut - rida * 9 - 1

    if loendur == 0 and järjend_püsiv[rida][element] == 0 and järjend[rida][element] == 0:
        loendur = 1
        rida_1 = tahvel.create_line(x_koordinaadid[rida][element], y_koordinaadid[rida][element], x_koordinaadid[rida][element] + 50, y_koordinaadid[rida][element], width = 3.0, fill="red")
        rida_2 = tahvel.create_line(x_koordinaadid[rida][element], y_koordinaadid[rida][element], x_koordinaadid[rida][element], y_koordinaadid[rida][element] + 50, width = 3.0, fill="red")
        rida_3 = tahvel.create_line(x_koordinaadid[rida][element] + 50, y_koordinaadid[rida][element], x_koordinaadid[rida][element] + 50, y_koordinaadid[rida][element] + 50, width = 3.0, fill="red")
        rida_4 = tahvel.create_line(x_koordinaadid[rida][element], y_koordinaadid[rida][element] + 50, x_koordinaadid[rida][element] + 50, y_koordinaadid[rida][element] + 50, width = 3.0, fill="red")


        aken = Toplevel()
        aken.title("Number")
        aken.geometry("250x100")
        aken.protocol("WM_DELETE_WINDOW", sulgemine)
        silt = Label(aken, text = "Sisesta number: ", font=juhendi_font)
        silt.grid(column=0, row=0, padx=5, pady=5)

        teksti_kast = Entry(aken)
        teksti_kast.grid(column=1, row=0, padx=5, pady=5)

        nupp = Button(aken, text="OK", command=prindi_number)
        nupp.grid(column=0, row=1, columnspan=2, padx=5, pady=5)

def parem(event):
    #Kustutamine
    global järjend
    global järjend_püsiv

    vajutus = tahvel.find_withtag(CURRENT)[0]
    if vajutus > 85:
        silt = vajutus
        vajutatud_ruut = silt - 85
    else:
        vajutatud_ruut = vajutus
        silt = vajutatud_ruut + 85

    if vajutatud_ruut % 9 == 0:
        rida = vajutatud_ruut // 9 - 1
        element = 8
    else:
        rida = vajutatud_ruut // 9
        element = vajutatud_ruut - rida * 9 - 1

    if järjend[rida][element] != 0 and järjend_püsiv[rida][element] == 0:

        tahvel.itemconfigure(silt, text="")
        järjend[rida][element] = 0

def sudoku_genereerimine(tase):
    f=open("sudokud.txt")
    sudokud=f.read().splitlines()
    pikkus=len(sudokud)

    suvaline_sudoku=randint(0,pikkus/9-1)
    algus=suvaline_sudoku*9
    lõpp=9+suvaline_sudoku*9
    sudoku=[]

    while algus<lõpp:
        sudoku.append(sudokud[algus])
        algus+=1

    #Reaplokkide vahetamine
    reaplokid=[]


    def tekita_plokk(arv,järjend):
        plokk=[]
        for i in range(arv,arv+3):
            plokk.append(järjend[i])
        return plokk

    reaplokid.append(tekita_plokk(0,sudoku))
    reaplokid.append(tekita_plokk(3,sudoku))
    reaplokid.append(tekita_plokk(6,sudoku))

    def shuffle(järjend):
        pikkus=len(järjend)
        uus_järjend=[]
        for i in range(pikkus):
            uus_järjend.append(i)
        kasutatud_indeksid=[]
        for i in järjend:
            a=randint(0,pikkus-1)
            while a in kasutatud_indeksid:
                a=randint(0,pikkus-1)
            kasutatud_indeksid.append(a)
            uus_järjend[a]=i
        return uus_järjend

    veerud_või_plokid=randint(1,2)

    reaplokid_uus=shuffle(reaplokid)

    #Ridade vahetamine ploki sees
    järjend=reaplokid_uus[0]
    reaplokid_uus[0]=shuffle(järjend)
    järjend=reaplokid_uus[1]
    reaplokid_uus[1]=shuffle(järjend)
    järjend=reaplokid_uus[2]
    reaplokid_uus[2]=shuffle(järjend)

    def plokkide_eemaldamine(plokid,mingi_järjend):
        for i in plokid:
            for k in range(3):
                mingi_järjend.append(i[k])
        return mingi_järjend

    sudoku=[]
    sudoku=plokkide_eemaldamine(reaplokid_uus,sudoku)

    #Veerud:
    veerud=[]
    for i in range(9):
        veerg=[]
        for k in sudoku:
            veerg.append(k[i])
        veerg="".join(veerg)
        veerud.append(veerg)

    veeruplokid=[]

    veeruplokid.append(tekita_plokk(0,veerud))
    veeruplokid.append(tekita_plokk(3,veerud))
    veeruplokid.append(tekita_plokk(6,veerud))

    #Veeruplokkide vahetamine
    veeruplokid_uus=shuffle(veeruplokid)


    #Veergude vahetamine ploki sees
    järjend=veeruplokid_uus[0]
    veeruplokid_uus[0]=shuffle(järjend)
    järjend=veeruplokid_uus[1]
    veeruplokid_uus[1]=shuffle(järjend)
    järjend=veeruplokid_uus[2]
    veeruplokid_uus[2]=shuffle(järjend)

    #Selguse mõttes lähen uuesti üle ridadele
    veerud=[]
    veerud=plokkide_eemaldamine(veeruplokid_uus,veerud)

    read=[]
    for i in range(9):
        rida=[]
        for veerg in veerud:
            rida.append(veerg[i])
        rida="".join(rida)
        read.append(rida)

    #Kahe numbri omavahel vahetamine

    sudoku=[]
    a=randint(1,9)
    b=randint(1,9)
    while b==a:
        b=randint(1,9)
    for rida in read:
        rida=list(rida)
        a=str(a)
        b=str(b)
        indeks1=rida.index(a)
        indeks2=rida.index(b)
        rida[indeks1]=b
        rida[indeks2]=a
        sudoku.append(rida)


    #Muudan osad numbrid nullideks
    #Raskustaseme järgi võiks igast reast 3-4; 4-5 või 5-6 numbrit ära kustutada

    if tase=="Kerge":
        x=3
        y=4
    elif tase=="Keskmine":
        x=4
        y=5
    elif tase=="Raske":
        x=5
        y=6
    elif tase=="Asian":
        x=6
        y=7
    for rida in sudoku:
        kasutatud_indeksid=[]
        a=randint(x,y)#Muudab numbreid nulliks nii mitu korda
        for k in range(a):
            indeks=randint(0,8)
            while indeks in kasutatud_indeksid:
                indeks=randint(0,8)
            rida[indeks]=0
            kasutatud_indeksid.append(indeks)

    return sudoku

def kontrolli_sudoku(tabel):
    global tulemuse_id
    def veerg_on_korras(veerud, veeru_indeks):
        if not sorted(veerud[veeru_indeks]) == sudoku_arvulist:
            return False
        return True

    def rida_on_korras(tabel, rea_indeks):
        if not sorted(tabel[rea_indeks]) == sudoku_arvulist:
            return False
        return True

    def ruut_3x3_on_korras(tabel, alg_rea_indeks, alg_veeru_indeks):
        ruut = []
        for a in range(3):
            for b in range(3):
                ruut.append(tabel[alg_rea_indeks + a][alg_veeru_indeks + b])
        if not sorted(ruut) == sudoku_arvulist:
            return False
        return True

    def kontrolli():
        for i in range(9):
            if 0 in tabel[i]:
                return "Sudoku on lõpetamata."
        for i in range(9):
            if not veerg_on_korras(veerud, i):
                vigane = i
                return "Viga esineb " + str(vigane + 1) + ". veerus."
            elif not rida_on_korras(tabel, i):
                vigane = i
                return "Viga esineb " + str(vigane + 1) + ". reas."
        ruut = 0
        for i in range(0,9,3):
            for j in range(0,9,3):
                ruut += 1
                if not ruut_3x3_on_korras(tabel, i, j):
                    return "Viga esineb " + str(ruut) + ". 3x3 ruudus (nummerdades vasakult paremale)."
        return "Tubli! Sudoku on õigesti lahendatud!"


    sudoku_arvulist = [1,2,3,4,5,6,7,8,9]

    for i in range(9):
        for j in range(9):
            tabel[i][j] = int(tabel[i][j])

    veerud = []
    for i in range(9):
        veerg = []
        for rida in tabel:
            veerg.append(rida[i])
        veerud.append(veerg)

    tekst = kontrolli()
    tahvel.itemconfigure(tulemuse_id, text=tekst)

def raami_tekitamine_kontrolliga(tase):

    #Fondid
    global numbrite_font
    global kontrolli_font
    global juhendi_font
    numbrite_font=font.Font(family="calibri",size=20,weight="bold")
    kontrolli_font=font.Font(family="calibri",size=12,weight="bold")
    juhendi_font=font.Font(family="calibri",size=10)

    #-----------------------------------------------------------------------------
    #Tahvli loomine, tervitus, juhend
    global raam
    global tahvel
    raam = Toplevel()
    raam.title("Sudoku")
    tahvel = Canvas(raam, width=550, height=650, background="white")
    tahvel.grid()
    tervitus = Label(tahvel, text = "Tere tulemast mängima Helena ja Mari Liisi sudokut! Edukat nuputamist!", font="calibri 13 bold", bg="white")
    tervitus.place(x=25, y=10)
    juhend1 = Label(tahvel, text = "Numbri sisestamiseks vajuta soovitud ruudule vasaku hiireklahviga.", font=juhendi_font, bg="white")
    juhend1.place(x=50, y=510)
    juhend2 = Label(tahvel, text="Numbri kustutamiseks vajuta soovitud ruudule parema hiireklahviga.", font=juhendi_font, bg="white")
    juhend2.place(x=50, y=530)

    global loendur
    loendur = 0

    #---------------------------------------------------------------------------
    #Ruudu ülemise vasaku nurga koordinaadid ridade kaupa
    global x_koordinaadid
    global y_koordinaadid
    x_koordinaadid = []
    y_koordinaadid = []
    #---------------------------------------------------------------------------
    #Ruudustiku loomine, bindimine
    x1 = 50
    y1 = 50
    x2 = 100
    y2 = 100
    for i in range(9):
        x_rida = []
        y_rida = []
        for i in range(9):
            x_rida += [x1]
            y_rida += [y1]

            ruut = joonista_ruut(x1, y1, x2, y2,tahvel)
            tahvel.tag_bind(ruut, "<Button-1>", vasak)
            tahvel.tag_bind(ruut, "<Button-3>", parem)
            x1 += 50
            x2 += 50

        x_koordinaadid += [x_rida]
        y_koordinaadid += [y_rida]

        y1 += 50
        y2 += 50
        x1 = 50
        x2 = 100

    #Paksud jooned
    tahvel.create_line(200, 50, 200, 500, width = 3.0)
    tahvel.create_line(350, 50, 350, 500, width = 3.0)
    tahvel.create_line(50, 200, 500, 200, width = 3.0)
    tahvel.create_line(50, 350, 500, 350, width = 3.0)

    #-----------------------------------------------------------------------------------------
    #Järjendi järgi numbrite ruutudesse kirjutamine
    global järjend
    global järjend_püsiv
    järjend = sudoku_genereerimine(tase)

    for i in range(9):
        for j in range(9):
            if järjend[i][j] != 0:
                tahvel.create_text(x_koordinaadid[i][j] + 25, y_koordinaadid[i][j] + 25, text=järjend[i][j], font=numbrite_font)
            else:
                silt = tahvel.create_text(x_koordinaadid[i][j] + 25, y_koordinaadid[i][j] + 25, text=None, font=numbrite_font)
                tahvel.tag_bind(silt, "<Button-3>", parem)

    järjend_püsiv = copy.deepcopy(järjend)
    #-----------------------------------------------------------------------------------------
    #Kontrolli nupp + tulemuse tekst
    global tulemuse_id
    kontroll = Button(tahvel, text="Kontrolli!", font=kontrolli_font, command=lambda: kontrolli_sudoku(järjend))
    kontroll.place(x = 235, y = 560)

    tulemuse_id = tahvel.create_text(275, 610, text=None, font=kontrolli_font)

    raam.mainloop()

def tase_ja_programmi_jooksutamine():
    global Raam

    Raam=Tk()
    Raam.title("Tase")

    kerge=Button(Raam,text="Kerge",command=lambda: raami_tekitamine_kontrolliga("Kerge"))
    kerge.grid(column=1,row=0,padx=5,pady=5,sticky=(S,W,E))

    keskmine=Button(Raam,text="Keskmine",command=lambda: raami_tekitamine_kontrolliga("Keskmine"))
    keskmine.grid(column=2,row=0,padx=5,pady=5,sticky=(S,W,E))

    raske=Button(Raam,text="Raske",command=lambda: raami_tekitamine_kontrolliga("Raske"))
    raske.grid(column=3,row=0,padx=5,pady=5,sticky=(S,W,E))

    asiaat=Button(Raam,text="Asian",command=lambda: raami_tekitamine_kontrolliga("Asian"))
    asiaat.grid(column=4,row=0,padx=5,pady=5,sticky=(S,W,E))
    Raam.mainloop()

tase_ja_programmi_jooksutamine()