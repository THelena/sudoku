def veerg_on_korras(veerud, veeru_indeks):
    if not sorted(veerud[veeru_indeks]) == sudoku_arvulist:
        return False
    return True

def rida_on_korras(tabel, rea_indeks):
    if not sorted(tabel[rea_indeks]) == sudoku_arvulist:
        return False
    return True

def ruut_3x3_on_korras(tabel, alg_rea_indeks, alg_veeru_indeks):
    ruut = []
    for a in range(3):
        for b in range(3):
            ruut.append(tabel[alg_rea_indeks + a][alg_veeru_indeks + b])
    if not sorted(ruut) == sudoku_arvulist:
        return False
    return True

def kontrolli():
    for i in range(9):
        if not veerg_on_korras(veerud, i):
            vigane = i
            return "Viga esineb " + str(vigane + 1) + " veerus"
        elif not rida_on_korras(tabel, i):
            vigane = i
            return "Viga esineb " + str(vigane + 1) + " reas"
    for i in range(0,9,3):
        for j in range(0,9,3):
            if not ruut_3x3_on_korras(tabel, i, j):
                vigane_rida = i
                vigane_veerg = j
                return "Viga esineb 3x3 ruudus, mille loodenurgas olev ruut asub " + str(vigane_rida + 1) + " reas ja " + str(vigane_veerg + 1) + " veerus"
    return "OK"


sudoku_arvulist = [1,2,3,4,5,6,7,8,9]

tabel = [['8', '2', '9', '5', '6', '4', '1', '3', '7'],
['3', '5', '7', '9', '2', '1', '6', '4', '8'],
['6', '4', '1', '8', '7', '3', '9', '5', '2'],
['5', '8', '6', '1', '9', '7', '4', '2', '3'],
['2', '1', '4', '3', '5', '6', '7', '8', '9'],
['7', '9', '3', '2', '4', '8', '5', '6', '1'],
['4', '6', '8', '7', '1', '2', '3', '9', '5'],
['9', '7', '2', '4', '3', '5', '8', '1', '6'],
['1', '3', '5', '6', '8', '9', '2', '7', '4']]

for i in range(9):
    for j in range(9):
        tabel[i][j] = int(tabel[i][j])

veerud = []
for i in range(9):
    veerg = []
    for rida in tabel:
        veerg.append(rida[i])
    veerud.append(veerg)


print(kontrolli())